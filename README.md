DS - PR1
____________________________________________

Sobre la solución propuesta por el profesor se han añadido los siguientes recursos:

Nuevo alcance:
- Modelos de entidades: main/java/uoc/ds/pr/model
  - File: Modelo que implementa las fichas creadas.
  - OrganizingEntity: Modelo que implementa las Entiaddes Organizadoras.
  - Player: Modelo que implementa los participantes de los eventos.
  - Rating: Modelo que implementa las valoraciones de los eventos.
  - SportEvent: Modelo que implementa los eventos deportivos.
- OrderedVector: main/java/uoc/ds/pr/util/OrderedVector.java
  - Permite insertar y reordenar los elementos de un array atendiendo al criterio especificado en la práctica.
- SportEvents4ClubImpl: main/java/uoc/ds/pr/SportEvents4ClubImpl.java
  - Implementa la interfaz propuesta, así como las distintas estructuras para gestionar la aplicación siguiendo los modelos propuestos en la solución de la PEC1.
- OrderedVectorTest: test/java/uoc/ds/pr/util/OrderedVectorTest.java
  - Clase que implementa los test unitarios de la clase OrderedVector.
- SportEventsClubPR1AdditionalTest: test/java/uoc/ds/pr/SportEventsClubPR1AdditionalTest.java
  - Ampliación de la cobertura de test propuestos por el profesor.
- Excepciones: /main/java/uoc/ds/pr/exceptions
  - Implementa todas las excepciones definidas en la propuesta de la práctica.

Actualizaciones del diseño:
- Se ha implementando una constante fecha en los Test propuestos en el enunciado empleada para fijar la fecha inicio y fin de los eventos. Esta medida permite pasar los test a través de la implementación correcta de los métodos. En caso contrario se permitiría inscribirse en eventos que ya habían comenzado o habían acabado.


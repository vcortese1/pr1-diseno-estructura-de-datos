package uoc.ds.pr.util;

import uoc.ds.pr.SportEvents4Club;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class ResourceUtil {

    public static byte getFlag(byte... args){
        Integer flag = 0;
        for(byte arg: args){
            String binary = getBinary(arg);
            flag += Integer.valueOf(binary);
        }
        return (byte) Integer.parseInt(flag.toString(),2);
    }

    public static boolean hasPublicSecurity(byte resource){
        String binary = getBinary(resource);
        String targetFlagBinary = getBinary(SportEvents4Club.FLAG_PUBLIC_SECURITY);
        int targetIndex = targetFlagBinary.indexOf('1');
        return binary.charAt(targetIndex) == targetFlagBinary.charAt(targetFlagBinary.indexOf('1'));
    }

    public static boolean hasPrivateSecurity(byte resource){
        String binary = getBinary(resource);
        String targetFlagBinary = getBinary(SportEvents4Club.FLAG_PRIVATE_SECURITY);
        int targetIndex = targetFlagBinary.indexOf('1');
        return binary.charAt(targetIndex) == targetFlagBinary.charAt(targetFlagBinary.indexOf('1'));
    }

    public static boolean hasBasicLifeSupport(byte resource){
        String binary = getBinary(resource);
        String targetFlagBinary = getBinary(SportEvents4Club.FLAG_BASIC_LIFE_SUPPORT);
        int targetIndex = targetFlagBinary.indexOf('1');
        return binary.charAt(targetIndex) == targetFlagBinary.charAt(targetFlagBinary.indexOf('1'));
    }

    public static boolean hasVolunteers(byte resource){
        String binary = getBinary(resource);
        String targetFlagBinary = getBinary(SportEvents4Club.FLAG_VOLUNTEERS);
        int targetIndex = targetFlagBinary.indexOf('1');
        return binary.charAt(targetIndex) == targetFlagBinary.charAt(targetFlagBinary.indexOf('1'));
    }

    public static boolean hasAllOpts(byte resource){
        String binary = getBinary(resource);
        return binary.equals("1111");
    }

    private static String getBinary(byte arg){
        return String.format("%4s",Integer.toBinaryString(arg & 0xFFFF)).replace(' ','0');
    }
}

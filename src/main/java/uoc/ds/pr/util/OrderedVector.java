package uoc.ds.pr.util;

import edu.uoc.ds.adt.sequential.Container;
import edu.uoc.ds.traversal.Iterator;
import edu.uoc.ds.traversal.IteratorArrayImpl;
import uoc.ds.pr.model.Rating;
import uoc.ds.pr.model.SportEvent;

import java.util.Arrays;

public class OrderedVector<E> implements Container<E> {

    protected E[] elems;

    private int n;

    public OrderedVector(int max) {
        this.elems = (E[]) new Object[max];
        this.n = 0;
    }

    @Override
    public boolean isEmpty() {
        return n == 0;
    }

    @Override
    public int size() {
        return n;
    }

    @Override
    public Iterator<E> values() {
        return new IteratorArrayImpl(this.elems, this.n, 0);
    }

    public void add(E elem){
        elems[n] = elem;
        n++;
        reorder();
    }

    public void reorder() {
        for (int x = 0; x < n; x++) {
            // Aquí "y" se detiene antes de llegar
            // a length - 1 porque dentro del for, accedemos
            // al siguiente elemento con el índice actual + 1
            for (int y = 0; y < n - 1; y++) {
                SportEvent elementoActual = (SportEvent) elems[y],
                  elementoSiguiente = (SportEvent) elems[y + 1];
                if (elementoActual.rating() < elementoSiguiente.rating()) {
                    // Intercambiar
                    elems[y] = (E) elementoSiguiente;
                    elems[y + 1] = (E) elementoActual;
                }
            }
        }
    }

    public E first(){
        return (n > 0) ? elems[0] : null;
    }
}

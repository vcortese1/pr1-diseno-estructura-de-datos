package uoc.ds.pr.model;

import edu.uoc.ds.adt.sequential.LinkedList;
import edu.uoc.ds.traversal.Iterator;

import java.time.LocalDate;
import java.util.Arrays;

import static uoc.ds.pr.SportEvents4Club.MAX_NUM_SPORT_EVENTS;

public class Player {
    private String id;
    private String name;
    private String surname;
    private LocalDate dateOfBirth;
    private LinkedList<SportEvent> events;
    public static Player mostActivePlayer;
    private int[] inscriptionOrder;

    public Player(String id, String name, String surname, LocalDate dateOfBirth) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
        events = new LinkedList<>();
        // Este simple array almacenara la posición de inscripción en las distintas listas de los eventos.
        // Será muy útil para la comparación con otro jugador por el más activo
        inscriptionOrder = new int[MAX_NUM_SPORT_EVENTS];
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getId() {
        return id;
    }

    public int numEvents(){
        return events.size();
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public LinkedList<SportEvent> getEvents() {
        return events;
    }

    public int[] getInscriptionOrder() {
        return inscriptionOrder;
    }

    public Player compareMostActive(){
        boolean condition1 = (mostActivePlayer != null && this.getEvents().size() > mostActivePlayer.getEvents().size()),
                condition2 = (mostActivePlayer != null && this.compareDateInscriptions(mostActivePlayer)),
                condition3 = mostActivePlayer == null;
        return ( condition3 || condition1 || condition2) ? this : mostActivePlayer;
    }

    public boolean isAlreadySigned(String eventId){
        boolean signed = false;
        Iterator<SportEvent> it = events.values();
        while(it.hasNext()) {
            SportEvent i = it.next();
            if(i.getEventId().equals(eventId)) {
               signed = true;
            }
        }
        return signed;
    }

    public boolean compareDateInscriptions(Player player){
        int rank1 = Arrays.stream(player.inscriptionOrder).sum(), rank2 = Arrays.stream(this.inscriptionOrder).sum();
        // Almacenamos las sumas de las posiciones en las que se inscribieron a los distintos eventos
        // Aplicando el sentido lógico, el jugador cuya suma sea menor, implicará que se ha apuntado con mayor anterioridad a los eventos.
        return (rank1 < rank2) ? false : true;
    }
}

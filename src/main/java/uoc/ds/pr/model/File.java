package uoc.ds.pr.model;

import uoc.ds.pr.SportEvents4Club;

import java.time.LocalDate;

public class File {
    private String fileId;
    private SportEvent event;
    private OrganizingEntity organizingEntity;
    private SportEvents4Club.Status status;
    private LocalDate updateDate;
    private String description;
    public static int rejectedFiles;
    public static int totalFiles;

    public File(String fileId, SportEvent event, OrganizingEntity organizingEntity, SportEvents4Club.Status status) {
        this.fileId = fileId;
        this.event = event;
        this.organizingEntity = organizingEntity;
        this.status = status;
    }

    public String getFileId() {
        return fileId;
    }

    public SportEvents4Club.Status getStatus() {
        return status;
    }

    public void setUpdateDate(LocalDate updateDate) {
        this.updateDate = updateDate;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public OrganizingEntity getOrganizingEntity() {
        return organizingEntity;
    }

    public SportEvent getEvent() {
        return event;
    }
}

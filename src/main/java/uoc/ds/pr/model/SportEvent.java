package uoc.ds.pr.model;

import edu.uoc.ds.adt.sequential.LinkedList;
import edu.uoc.ds.adt.sequential.QueueArrayImpl;
import edu.uoc.ds.traversal.Iterator;
import uoc.ds.pr.SportEvents4Club;
import uoc.ds.pr.SportEvents4ClubImpl;

import java.time.LocalDate;

import static uoc.ds.pr.SportEvents4Club.MAX_NUM_ENROLLMENT;

public class SportEvent {
    private String eventId;
    private String description;
    private SportEvents4Club.Type type;
    private byte resources;
    private int max;
    private LocalDate startDate;
    private LocalDate endDate;
    private LinkedList<Rating> rating;
    private QueueArrayImpl<Player> inscriptions;

    public SportEvent(String eventId, String description, SportEvents4Club.Type type, byte resources, int max, LocalDate startDate, LocalDate endDate) {
        this.eventId = eventId;
        this.description = description;
        this.type = type;
        this.resources = resources;
        this.max = max;
        this.startDate = startDate;
        this.endDate = endDate;
        rating = new LinkedList<>();
        inscriptions = new QueueArrayImpl<>(MAX_NUM_ENROLLMENT);
    }

    public String getEventId() {
        return eventId;
    }

    public float rating() {
        float totalPoints = 0;
        Iterator<Rating> it = rating.values();
        while(it.hasNext()) {
            Rating i = it.next();
            totalPoints += i.rating().getValue();
        }
        // Prevenimos la división por cero
        return (rating.size() > 0) ? totalPoints / rating.size() : 0;
    }

    public QueueArrayImpl<Player> getInscriptions() {
        return inscriptions;
    }

    public int getMax() {
        return max;
    }

    public LinkedList<Rating> getRating() {
        return rating;
    }

    public boolean hasVacancies(){
        return (inscriptions.size() > max) ? false : true;
    }

    public boolean isEnded(){
        boolean isEnded = (LocalDate.now().isBefore(endDate)) ? false : true;
        return isEnded ;
    }

    public boolean isStarted(){
        boolean isStarted = (LocalDate.now().isAfter(startDate)) ? true : false;
        return isStarted;
    }

    public void setMax(int max) {
        this.max = max;
    }
}

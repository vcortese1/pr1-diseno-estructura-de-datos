package uoc.ds.pr.model;

import edu.uoc.ds.adt.sequential.LinkedList;

public class OrganizingEntity {
    private int id;
    private String name;
    private String description;
    private LinkedList<SportEvent> events;

    public OrganizingEntity(int organizingEntityId, String name, String description) {
        this.id = organizingEntityId;
        this.name = name;
        this.description = description;
        events = new LinkedList<>();
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LinkedList<SportEvent> getEvents() {
        return events;
    }
}

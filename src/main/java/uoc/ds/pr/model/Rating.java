package uoc.ds.pr.model;

import uoc.ds.pr.SportEvents4Club;

public class Rating {
    private SportEvents4Club.Rating valuation;
    private String comment;
    private Player player;

    public Rating(SportEvents4Club.Rating valuation, String comment, Player player) {
        this.valuation = valuation;
        this.comment = comment;
        this.player = player;
    }

    public SportEvents4Club.Rating rating(){
        return valuation;
    }

    public Player getPlayer() {
        return player;
    }
}

package uoc.ds.pr;

import edu.uoc.ds.adt.sequential.DictionaryArrayImpl;
import edu.uoc.ds.adt.sequential.QueueArrayImpl;
import edu.uoc.ds.traversal.Iterator;
import uoc.ds.pr.exceptions.*;
import uoc.ds.pr.model.*;
import uoc.ds.pr.util.OrderedVector;

import java.time.LocalDate;

public class SportEvents4ClubImpl implements SportEvents4Club {

    private Player[] players;
    private static int lastPlayer;
    private OrganizingEntity[] organizingEntities;
    private static int lastOrganizingEntity;
    private DictionaryArrayImpl<String,SportEvent> sportEvents;
    private QueueArrayImpl<File> files;
    private OrderedVector<SportEvent> bestEvents;

    public SportEvents4ClubImpl() {
        players = new Player[MAX_NUM_PLAYER];
        organizingEntities = new OrganizingEntity[MAX_NUM_ORGANIZING_ENTITIES];
        this.sportEvents = new DictionaryArrayImpl<>(MAX_NUM_SPORT_EVENTS);
        this.files = new QueueArrayImpl<>();
        this.bestEvents = new OrderedVector<>(MAX_NUM_SPORT_EVENTS);
        Player.mostActivePlayer = null;
        lastPlayer = 0;
        lastOrganizingEntity = 0;
        File.totalFiles = 0;
        File.rejectedFiles = 0;
    }

    @Override
    public void addPlayer(String id, String name, String surname, LocalDate dateOfBirth) {
        Player target = getPlayer(id);
        // Comprobamos si existe referencia al objeto jugador y si aún tenemos capacidad para almacenarlo
        if(target == null && lastPlayer < players.length){
            players[lastPlayer] = new Player(id,name,surname,dateOfBirth);
            lastPlayer ++; // Aumentamos el contador de jugadores almacenados
        } else {
            // Editamos el objeto jugador que hemos obtenido
            target.setName(name);
            target.setSurname(surname);
            target.setDateOfBirth(dateOfBirth);
        }
    }

    @Override
    public void addOrganizingEntity(int id, String name, String description) {
        OrganizingEntity target = getOrganizingEntity(id);
        // Comprobamos si existe referencia al objeto entidadOrganizadora y si aún tenemos capacidad para almacenarla
        if(target == null && lastOrganizingEntity < organizingEntities.length){
            organizingEntities[lastOrganizingEntity] = new OrganizingEntity(id,name,description);
            lastOrganizingEntity++; // Aumentamos el contador de entidades almacenadas
        } else {
            // Editamos el objeto entidadOrganizadora que hemos obtenido
            target.setName(name);
            target.setDescription(description);
        }
    }

    @Override
    public void addFile(String id, String eventId, int orgId, String description, Type type, byte resources, int max, LocalDate startDate, LocalDate endDate) throws OrganizingEntityNotFoundException {
        // Comprobamos que la entidad organizadora existe
        OrganizingEntity organizingEntity = getOrganizingEntity(orgId);
        if(organizingEntity == null) throw new OrganizingEntityNotFoundException();
        //Comprobamos que no existe un evento con el id facilitado. No se especifica que se deba devolver una excepción en caso de que ya exista
        if(!sportEvents.containsKey(eventId)){
            //Se crea el nuevo evento deportivo
            SportEvent newSportEvent = new SportEvent(eventId, description, type, resources, max, startDate, endDate);
            //Se crea una nueva ficha en nuestra colección
            files.add(new File(id,newSportEvent, organizingEntity, Status.PENDING));
            // Actualizamos el contador total de fichas
            File.totalFiles ++;
        }
    }

    @Override
    public File updateFile(Status status, LocalDate date, String description) throws NoFilesException {
        // Comprobamos en primer lugar si hay fichas pendientes de revisión
        if(files.isEmpty()) throw new NoFilesException(); // Lanzamos la excepción controlada si no existen
        else {
            File target = null;
            // Dependiendo del estado haremos distintas operaciones
            switch (status){
                case ENABLED -> {
                    // Si queremos ACEPTAR el evento procedemos a:
                    // - Obtenemos la ficha que mas tiempo lleva en la cola
                    // - La insertamos en la estructura que almacena los eventos de la entidad organizadora
                    // - La insertamos en la estructura que almacena todos los eventos deportivos
                    target = files.poll();
                    target.setUpdateDate(date);
                    target.setDescription(description);
                    // Importante reseñar que almacenamos en ambas estructuras el mismo objeto (dirección de memoria)
                    target.getOrganizingEntity().getEvents().insertEnd(target.getEvent());
                    sportEvents.put(target.getEvent().getEventId(), target.getEvent());
                }
                case PENDING -> {
                    // Si aún sigue PENDIENTE, pero queremos actualizar la información haremos un peek, que nos trae el elemento que mas tiempo lleva en la cola
                    // pero sin borrarlo de la misma
                    target = files.peek();
                    target.setUpdateDate(date);
                    target.setDescription(description);
                }
                case DISABLED -> {
                    // Si queremos RECHAZAR el evento, hacemos lo mismo que en la aprobación con la excepción de que nos insertamos el evento en el resto de estructuras
                    // Además actualizamos el contador de fichas rechazadas.
                    target = files.poll();
                    target.setUpdateDate(date);
                    target.setDescription(description);
                    File.rejectedFiles ++;
                }
            }
            return target;
        }
    }

    @Override
    public void signUpEvent(String playerId, String eventId) throws PlayerNotFoundException, SportEventNotFoundException, LimitExceededException {
        // Comprobaciones de la pre condición del método
        Player targetPlayer = getPlayer(playerId);
        if(targetPlayer == null) throw new PlayerNotFoundException();
        SportEvent targetEvent = getSportEvent(eventId);
        if(targetEvent == null) throw new SportEventNotFoundException();
        // Comprobamos que no se hayan cubierto todas las plazas totales disponibles del evento y que este no este comenzado o finalizado
        if(!targetEvent.getInscriptions().isFull() && !targetEvent.isEnded() && !targetEvent.isStarted()){
            // El jugador se almacena en el evento aunque no existan vacantes (quedará en la reserva)
            targetEvent.getInscriptions().add(targetPlayer);
            // Se lanza la excepción controlada si no hay vacantes
            if(!targetEvent.hasVacancies()) throw new LimitExceededException();
            // Si hay vacantes procedemos a inscribirlo en la colección del jugador
            else{
                // Comprobamos que el jugador no tenga ya añadido el evento
                if(!targetPlayer.isAlreadySigned(eventId)) {
                    targetPlayer.getInscriptionOrder()[targetPlayer.getEvents().size()] = targetEvent.getInscriptions().size();
                    targetPlayer.getEvents().insertEnd(targetEvent);
                    // Realizamos la comparación de este jugador con el más activo para ver si debe ser sustituido o no
                    Player.mostActivePlayer = targetPlayer.compareMostActive();
                }
            }
        }
    }

    @Override
    public double getRejectedFiles() {
        // Se debe hacer un casting para que el resultado no sea 0
        return (double) File.rejectedFiles / (double) File.totalFiles;
    }

    @Override
    public Iterator<SportEvent> getSportEventsByOrganizingEntity(int organizationId) throws NoSportEventsException {
        OrganizingEntity target = getOrganizingEntity(organizationId);
        if(target == null) throw new NoSportEventsException();
        Iterator<SportEvent> sportEvents = null;
        // Si no existe la entidad organizadora no haremos nada (No se especifica que debamos lanzar una excepción)
        if(target != null){
            // Si la empresa no tiene eventos almacenados devolvemos la excepción controlada
            if(target.getEvents().isEmpty()) throw new NoSportEventsException();
            else sportEvents = target.getEvents().values();
        }
        return sportEvents;
    }

    @Override
    public Iterator<SportEvent> getAllEvents() throws NoSportEventsException {
        if (sportEvents.isEmpty()) throw new NoSportEventsException();
        else return sportEvents.values();
    }

    @Override
    public Iterator<SportEvent> getEventsByPlayer(String playerId) throws NoSportEventsException {
        Player target = getPlayer(playerId);
        Iterator<SportEvent> playerEvents = null;
        // Si no existe el jugador no haremos nada (No se especifica que debamos lanzar una excepción)
        if (target != null){
            // Si el jugador no tiene eventos almacenados devolvemos la excepción controlada
            if(target.getEvents().isEmpty()) throw new NoSportEventsException();
            else playerEvents =  target.getEvents().values();
        }
        return playerEvents;
    }

    @Override
    public void addRating(String playerId, String eventId, Rating rating, String message) throws SportEventNotFoundException, PlayerNotFoundException, PlayerNotInSportEventException {
        SportEvent targetEvent = getSportEvent(eventId);
        // Pre-condicion de existencia del evento
        if(targetEvent == null) throw new SportEventNotFoundException();
        // Pre-condicion de existencia del jugador
        Player targetPlayer = getPlayer(playerId);
        // Pre-condicion de participación del jugador en el evento y además que el evento haya finalizado
        if(targetPlayer == null) throw new PlayerNotFoundException();
        if(!targetPlayer.isAlreadySigned(eventId)) throw new PlayerNotInSportEventException();
        else{
            targetEvent.getRating().insertEnd(new uoc.ds.pr.model.Rating(rating,message, targetPlayer));
            bestEvents.add(targetEvent);
        }
    }

    @Override
    public Iterator<uoc.ds.pr.model.Rating> getRatingsByEvent(String eventId) throws SportEventNotFoundException, NoRatingsException {
        SportEvent target = getSportEvent(eventId);
        Iterator<uoc.ds.pr.model.Rating> ratings = null;
        // Si el evento no esta almacenado devolvemos la excepción controlada
        if(target == null) throw new SportEventNotFoundException();
        else{
            // Si el evento seleccionado no tiene valoraciones devolvemos la excepción controlada
            if(target.getRating().isEmpty()) throw new NoRatingsException();
            else ratings = target.getRating().values();
        }
        return ratings;
    }

    @Override
    public Player mostActivePlayer() throws PlayerNotFoundException {
        if(Player.mostActivePlayer == null) throw new PlayerNotFoundException();
        return Player.mostActivePlayer;
    }

    @Override
    public SportEvent bestSportEvent() throws SportEventNotFoundException {
        if(bestEvents.isEmpty()) throw new SportEventNotFoundException();
        return bestEvents.first();
    }

    @Override
    public int numPlayers() {
        return lastPlayer;
    }

    @Override
    public int numOrganizingEntities() {
        return lastOrganizingEntity;
    }

    @Override
    public int numFiles() {
        return File.totalFiles;
    }

    @Override
    public int numRejectedFiles() {
        return File.rejectedFiles;
    }

    @Override
    public int numPendingFiles() {
        return files.size();
    }

    @Override
    public int numSportEvents() {
        return sportEvents.size();
    }

    @Override
    public int numSportEventsByPlayer(String playerId) {
        Player target = getPlayer(playerId);
        // No se especifican excepciones en la pre-condición. Por lo tanto debemos validar que el jugador exista y que el número de eventos en los que participa sea mayor que 0
        return (target != null && target.numEvents() > 0) ? target.numEvents() : 0;
    }

    @Override
    public int numPlayersBySportEvent(String sportEventId) {
        SportEvent target = getSportEvent(sportEventId);
        // No se especifican excepciones en la pre-condición. Por lo tanto debemos validar que el evento exista y que el número de inscripciones sea mayor que 0
        return (target != null && target.getInscriptions().size() > 0) ? target.getInscriptions().size() : 0;
    }

    @Override
    public int numSportEventsByOrganizingEntity(int orgId) {
        OrganizingEntity target = getOrganizingEntity(orgId);
        // No se especifican excepciones en la pre-condición. Por lo tanto debemos validar que la organización exista y que el número de eventos organizados sea mayor que 0
        return (target != null && target.getEvents().size() > 0) ? target.getEvents().size() : 0;
    }

    @Override
    public int numSubstitutesBySportEvent(String sportEventId) {
        SportEvent target = getSportEvent(sportEventId);
        // No se especifican excepciones en la pre-condición. Por lo tanto debemos validar que el evento exista y que el número de inscripciones supere el máximo de participantes del evento.
        return (target != null && !target.hasVacancies()) ? (target.getInscriptions().size() - target.getMax()) : 0;
    }

    @Override
    public Player getPlayer(String playerId) {
        Player target = null;
        for(int i = 0; i< lastPlayer && lastPlayer < players.length; i++){
            if(players[i].getId().equals(playerId)) target = players[i];
        }
        return target;
    }

    @Override
    public SportEvent getSportEvent(String eventId) {
        SportEvent target = null;
        if(sportEvents.containsKey(eventId)) target = sportEvents.get(eventId);
        return target;
    }

    @Override
    public OrganizingEntity getOrganizingEntity(int id) {
        OrganizingEntity target = null;
        for(int i = 0; i< lastOrganizingEntity && lastOrganizingEntity < organizingEntities.length; i++){
            if(organizingEntities[i].getId() == id) target = organizingEntities[i];
        }
        return target;
    }

    @Override
    public File currentFile() {
        // Se devuelve el elemento más antiguo de la cola de archivos.
        return files.peek();
    }
}

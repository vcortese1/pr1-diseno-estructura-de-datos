package uoc.ds.pr.util;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import uoc.ds.pr.FactorySportEvents4Club;
import uoc.ds.pr.SportEvents4Club;
import uoc.ds.pr.exceptions.PlayerNotFoundException;
import uoc.ds.pr.exceptions.SportEventNotFoundException;
import uoc.ds.pr.model.Rating;
import uoc.ds.pr.model.SportEvent;

import java.time.LocalDate;

import static uoc.ds.pr.SportEvents4Club.MAX_NUM_SPORT_EVENTS;

public class OrderedVectorTest {

    private OrderedVector<SportEvent> orderedVector;

    private SportEvents4Club sportEvents4Club;

    @Before
    public void setUp() throws Exception {
        this.sportEvents4Club = FactorySportEvents4Club.getSportEvents4Club();
        this.orderedVector = new OrderedVector<>(MAX_NUM_SPORT_EVENTS);
        SportEvent sportEvent1 = new SportEvent("event1","event1",
                SportEvents4Club.Type.MICRO,
                ResourceUtil.getFlag(SportEvents4Club.FLAG_ALL_OPTS),
                50,
                LocalDate.now().minusDays(2),
                LocalDate.now().minusDays(2));
        Rating rating1 = new Rating(SportEvents4Club.Rating.ONE,"rating comment", this.sportEvents4Club.getPlayer("idPlayer1"));
        sportEvent1.getRating().insertEnd(rating1);
        orderedVector.add(sportEvent1);
    }

    @Test
    public void isEmptyTest() throws SportEventNotFoundException {
        Assert.assertFalse(orderedVector.isEmpty());
    }

    @Test
    public void sizeTest() throws SportEventNotFoundException {
        Assert.assertEquals(1, orderedVector.size());
    }

    @Test
    public void valuesTest() throws SportEventNotFoundException {
        Assert.assertNotNull(orderedVector.values());
        Assert.assertEquals("event1", orderedVector.values().next().getEventId());
    }

    @Test
    public void reorderTest() throws SportEventNotFoundException {
        SportEvent sportEvent2 = new SportEvent("event2","event2",
                SportEvents4Club.Type.MICRO,
                ResourceUtil.getFlag(SportEvents4Club.FLAG_ALL_OPTS),
                50,
                LocalDate.now().minusDays(2),
                LocalDate.now().minusDays(2));
        Rating rating1 = new Rating(SportEvents4Club.Rating.FIVE,"rating comment", this.sportEvents4Club.getPlayer("idPlayer1"));
        sportEvent2.getRating().insertEnd(rating1);
        orderedVector.add(sportEvent2);
        orderedVector.reorder();
        Assert.assertEquals("event2", orderedVector.first().getEventId());
    }
}

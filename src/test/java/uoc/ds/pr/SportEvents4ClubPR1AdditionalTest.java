package uoc.ds.pr;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import uoc.ds.pr.exceptions.*;
import uoc.ds.pr.util.ResourceUtil;

import java.time.LocalDate;

public class SportEvents4ClubPR1AdditionalTest {

    private SportEvents4Club sportEvents4Club;

    @Before
    public void setUp() throws Exception {
        this.sportEvents4Club = FactorySportEvents4Club.getSportEvents4Club();
    }

    @Test
    //Test para validar que un jugador no puede evaluar un evento si este no esta inscrito
    public void validateEventCannotBeRatedIfNotSignedUp() throws PlayerNotInSportEventException, SportEventNotFoundException, PlayerNotFoundException {
        Assert.assertThrows(PlayerNotInSportEventException.class, ()->
                sportEvents4Club.addRating("idPlayer1","EV-1102", SportEvents4Club.Rating.FIVE, "rating comment"));
    }

    @Test
    //Test para validar que un evento no existente no puede ser evaluado
    public void validateEventCannotBeRatedIfNotExists() throws PlayerNotInSportEventException, SportEventNotFoundException, PlayerNotFoundException {
        Assert.assertThrows(SportEventNotFoundException.class, ()->
                sportEvents4Club.addRating("idPlayer1","EV-9999", SportEvents4Club.Rating.FIVE, "rating comment"));
    }

    @Test
    //Test para validar que un evento no puede ser evaluado si el evento no existe
    public void validateEventCannotBeRatedIfPlayerNotExists() throws PlayerNotInSportEventException, SportEventNotFoundException, PlayerNotFoundException {
        Assert.assertThrows(PlayerNotFoundException.class, ()->
                sportEvents4Club.addRating("idPlayer999","EV-1101", SportEvents4Club.Rating.FIVE, "rating comment"));
    }

    @Test
    //Test para validar que una ficha no puede ser creada si la organización organizadora no existe
    public void validateFileCannotBeInsertedIfOrganizationDoesNotExist(){
        Assert.assertThrows(OrganizingEntityNotFoundException.class, ()->sportEvents4Club.addFile("newFileId",
                "newEventId", 99, "desc", SportEvents4Club.Type.MICRO,
                ResourceUtil.getFlag(SportEvents4Club.FLAG_ALL_OPTS), 50, LocalDate.now(), LocalDate.now()));
    }

    @Test
    //Test para validar que no se cree una ficha si el evento ya existe (No se especifica lanzar una excepción, pero no debe crearse la ficha)
    public void validateFileCannotBeInsertedIfEventAlreadyExists() throws OrganizingEntityNotFoundException {
        int currentPendingFiles = sportEvents4Club.numPendingFiles();
        sportEvents4Club.addFile("newFileId",
                "EV-1101", 1, "desc", SportEvents4Club.Type.MICRO,
                ResourceUtil.getFlag(SportEvents4Club.FLAG_ALL_OPTS), 50, LocalDate.now(), LocalDate.now());
        Assert.assertEquals(currentPendingFiles, sportEvents4Club.numPendingFiles());
    }

    @Test
    //Test para validar que si njo hay archivos pendientes de aprobación se lanza una excepción
    public void validateFilesCannotByUpdatedIfIsEmpty() throws NoFilesException {
        // Previamente rechazamos el archivo pendiente
        sportEvents4Club.updateFile(SportEvents4Club.Status.DISABLED,LocalDate.now(),"disabled for testing");
        Assert.assertThrows(NoFilesException.class, ()->sportEvents4Club.updateFile(SportEvents4Club.Status.ENABLED,LocalDate.now(),"enable"));
    }

    @Test
    public void validPlayerCannotSigUpIfNotExists(){
        Assert.assertThrows(PlayerNotFoundException.class, ()->sportEvents4Club.signUpEvent("idPlayer9999","EV-1101"));
    }

    @Test
    public void validPlayerCannotSigUpIfEventNotExists(){
        Assert.assertThrows(SportEventNotFoundException.class, ()->sportEvents4Club.signUpEvent("idPlayer1","EV-9999"));
    }

    @Test
    public void validPlayerSigUpButVacanciesAreExceeded(){
        sportEvents4Club.getSportEvent("EV-1102").setMax(3);
        Assert.assertThrows(LimitExceededException.class, ()->sportEvents4Club.signUpEvent("idPlayer1","EV-1102"));
    }

    @Test
    public void allEventsAreEmptyTest(){
        SportEvents4Club sportEvents4ClubAux = new SportEvents4ClubImpl();
        Assert.assertThrows(NoSportEventsException.class, ()->sportEvents4ClubAux.getAllEvents());
    }
}
